--------------------------------------------------------------------------------
--
-- Módulo       : Dictionary
--
--
-- Se recomienda importarlos de forma 'qualificada'. Ej:
--
-- import qualified Dictionary as Dic
--
--
--------------------------------------------------------------------------------

module Dictionary
    ( Clave
    , Dictionary
    , vacio
    , esVacio
    , anadir
    , existe
    , buscarClave
    , quitar
    , nombre
    , cambiarNombre
    , cantidad
    , claves
    ) where

type Clave = Int

type Dictionary = [(Clave, String)]

vacio :: Dictionary
vacio = []

esVacio :: Dictionary -> Bool
esVacio = null

anadir :: String -> Dictionary -> (Dictionary, Clave)
anadir s [] = ([(0, s)], 0)
anadir s ds@((i,_):d) = (dic, i+1)
    where dic = (i+1, s):ds

existe :: Dictionary -> Clave -> Bool
existe d k = k `elem` claves d

-- PRE: String existe
buscarClave :: Dictionary -> String -> Clave
buscarClave ((k,d):ds) s | d == s = k
                         | True = buscarClave ds s 

quitar :: Dictionary -> Clave -> Dictionary
quitar [] k = error "Diccionario vacío"
quitar ds@((i,s):d) k = if k == i then d else (:) (i,s) $ quitar d k

nombre :: Dictionary -> Clave -> Maybe String
nombre d k = if null res then Nothing else Just $ snd $ head res
    where
            res = filter ((== k) . fst) d

-- PRE: clave existe
cambiarNombre :: Dictionary -> Clave -> String -> Dictionary
cambiarNombre ((k,d):ds) c s | k == c = (k,s):ds
                             | True   = (k,d): cambiarNombre ds c s


cantidad :: Dictionary -> Int
cantidad d = length d

claves :: Dictionary -> [Clave]
claves = map fst
