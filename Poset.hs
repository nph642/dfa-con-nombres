--------------------------------------------------------------------------------
--
-- Módulo       : Poset
--
--
-- Se recomienda importarlos de forma 'qualificada'. Ej:
--
-- import qualified Poset as P
--
--
--------------------------------------------------------------------------------

module Poset
    ( Poset
    , vacio
--consultas
    , cargar
    , crear
    , esVacio
    , meq
    , buscarClave
    , claveExiste
    , claveInfo
    , minimales
    , maximales
--modificaciones
    , cambiarInfo
    , anadir
    , quitar
--exportaciones
    , exportar
    , expGrafo
    , imprimir
    ) where

import qualified Dictionary as Dic

-- Tradicionalmente, un Poset es un conjunto de elementos, y un
-- conjunto de relaciones. Nuestro conjunto de elementos será
-- representado por un diccionario, cuyas claves conforman las
-- relaciones. Las relaciones son pares de claves del diccionario.
--
--
-- Las operaciones sobre el poset están pensadas para mantenerse
-- en caso de que el diccionario agregue más características.
-- En tal caso, es posible que la operación anadir, requiera en
-- un futuro, más argumentos.

type Relations = [(Dic.Clave, Dic.Clave)]
data Poset = PS Dic.Dictionary Relations

-- El Poset vacío
vacio :: Poset
vacio = PS Dic.vacio []

--cargar :: FilePath -> IO [String]
cargar fp = do 
            s <- readFile fp
            return $ let l = lines s in 
                        crear (read $ l !! 0) (read $ l !! 1)


crear :: Dic.Dictionary -> Relations -> Poset
crear dic rel | posetvalido dic rel = (PS dic rel)
              | True                = error "Poset no válido :("

posetvalido :: Dic.Dictionary -> Relations -> Bool
posetvalido d r = refl d r && trans d r && antis d r
    where
            refl d r = and [(x,x) `elem` r | x <- Dic.claves d]
            trans _ r = and [(a,c) `elem` r | (a,b) <- r, (_,c) <- r
                , (b,c) `elem` r]
            antis _ r = and [not ((b,a) `elem` r) | (a,b) <- r, a /= b ]


esVacio :: Poset -> Bool
esVacio (PS dic _) = null dic

-- PRE: Ambas claves existen
meq :: Poset -> Dic.Clave -> Dic.Clave -> Bool
meq (PS _ r) a b  = not . null $ filter (\(x,y) -> x == a && y == b) r

buscarClave :: Poset -> String -> Dic.Clave
buscarClave (PS d _) s = Dic.buscarClave d s

claveExiste :: Poset -> Dic.Clave -> Bool
claveExiste (PS d _) c = Dic.existe d c

claveInfo :: Poset -> Dic.Clave -> Maybe String
claveInfo (PS d _) c = Dic.nombre d c

minimales :: Poset -> [Dic.Clave]
minimales (PS d r) = [x | x <- Dic.claves d, and $ map (check x) r]
    where  check x = (\(a,b) -> b /= x || a == x)

maximales :: Poset -> [Dic.Clave]
maximales (PS d r) = [x | x <- Dic.claves d, and $ map (check x) r]
    where check x = (\(a,b) -> a /= x || b == x)

cambiarInfo :: Poset -> Dic.Clave -> String -> Poset
cambiarInfo (PS d r) c s = PS (Dic.cambiarNombre d c s) r

anadir :: Poset -> String -> [Dic.Clave] -> [Dic.Clave] -> Poset
anadir (PS dic rel) s ms ns = PS nvodic (rel ++ nuevos)
    where
            nuevos = (key, key):menores ++ mayores
            menores = concat $ map (\x -> [(a,key) | (a,b) <- rel
                                          , b == x] ) ms'
            mayores = concat $ map (\x -> [(key, b) | (a,b) <- rel
                                          , a == x] ) ns'
            
            -- ms' es ms, pero sin redundancias.
            -- Ejemplo: ms = [0,1] pero (0,1) \in rel. Entonces ms' = [1]
            ms' = filter (\x -> and $ map (checkA x) ms) ms
            ns' = filter (\x -> and $ map (checkB x) ns) ns
            checkA x a = a == x || not ((x,a) `elem` rel)
            checkB x b = b == x || not ((b,x) `elem` rel)
            (nvodic, key) = Dic.anadir s dic

quitar :: Poset -> Dic.Clave -> Poset
quitar (PS d r) c = PS (Dic.quitar d c) newrel
    where
            newrel = filter (noaparece c) r
            noaparece c (a,b) = a /= c && b /= c

imprimir :: Poset -> IO ()
imprimir (PS dic rel) =  putStrLn $ "--Diccionario\n" ++ (show dic) ++
    "\n--Relaciones\n" ++ (show rel)

exportar :: Poset -> FilePath -> IO ()
exportar (PS dic rel) fp =  writeFile fp $ (show dic) ++ "\n" ++ (show rel)

expGrafo :: Poset -> FilePath -> IO ()
expGrafo (PS _ rel) fp = writeFile fp $ "digraph G {\n" ++ concat
    [show a ++ " -> " ++ show b ++ "\n" | (a,b) <- rel] ++ "}"
